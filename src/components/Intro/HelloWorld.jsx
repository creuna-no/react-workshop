import React from 'react';

// Class component
export class HelloWorld extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello Creuna, this is a React component</h1>
            </div>
        )
    }
}

// Stateless function component
// export const HelloWorld = () => (
//     <h1>Hello Creuna, this is a stateless function component</h1>
// )


export default HelloWorld
