import React from 'react';

const Icon = () => <span> Icon</span>

class Children extends React.Component {

    render() {
        return (
            <div>
                <Button>Click me</Button>

                <Button>
                    Click me
                    <Icon/>
                </Button>

                <Button children="Click me"/>
            </div>
        )
    }
}

class Button extends React.Component {

    render() {
        return (
            <button>{this.props.children}</button>
        )
    }
}

export default Children;
