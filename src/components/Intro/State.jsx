import React from 'react';

class State extends React.Component {

    constructor() {
        super();

        // Initialize state
        this.state = {
            txt: 'Hello again'
        }

        // Bind events
        this.update = this.update.bind(this);
    }

    update(e) {
        this.setState({txt: e.target.value})
    }

    render() {
        return (
            <div>
                <input onChange={this.update} />
                <h1>{this.state.txt}</h1>
            </div>
        )
    }
}

export default State;
