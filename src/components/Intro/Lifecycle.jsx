import React from 'react';
import ReactDOM from 'react-dom';

class Lifecycle extends React.Component {

    constructor() {
        super();
        this.update = this.update.bind(this);

        this.state = {
            clicked: 0
        }
    }

    update() {
        this.setState({ clicked: this.state.clicked + 1 });
    }

    componentWillMount() {
        // Access to state and props. No access to DOM
        console.log('Mounting');
    }

    render() {
        console.log('Rendering');
        return (
            <button
                style={{width: '100px', height: '100px', fontSize: '20px'}}
                onClick={this.update}>
                {this.state.clicked}
            </button>
        )
    }

    componentDidMount() {
        console.log('Mounted');
    }

    componentWillUnmount() {
        console.log('Bye!');
    }
}

class Wrapper extends React.Component {

    constructor() {
        super();
    }

    mount() {
        ReactDOM.render(<Lifecycle />, document.getElementById('wrapper'));
    }

    unmount() {
        ReactDOM.unmountComponentAtNode(document.getElementById('wrapper'));
    }

    render() {
        return (
            <div>
                <button onClick={this.mount.bind(this)}>Mount</button>
                <button onClick={this.unmount.bind(this)}>Unmount</button>
                <div id="wrapper"/>
            </div>
        )
    }
}

export default Wrapper
