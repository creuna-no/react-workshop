import React from 'react';

export class LifecycleUpdating extends React.Component {

    constructor() {
        super();

        this.state = {
            val: 0
        }

        this.update = this.update.bind(this);
    }

    update() {
        this.setState({val: this.state.val + 1}, () => {
            if(this.state.val >= 5 && this.state.val <= 10) {
                this.props.showAlert();
            } else {
                this.props.hideAlert();
            }
        });

    }

    componentWillReceiveProps(nextProps) {
        console.log('Next props: ', nextProps.alert);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
        //return nextState.val % 5 == 0;
    }

    render() {
        return (
            <div>
                <button style={{width: '100px', height: '100px', fontSize: '20px'}} onClick={this.update}>
                    {this.state.val}
                </button>
                {this.props.alert && (
                    <div>Warning!</div>
                )}
            </div>
        )
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Prev state: ', prevState.val);
    }
}

export default LifecycleUpdating
