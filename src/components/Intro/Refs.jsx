import React from 'react';
import ReactDOM from 'react-dom';

export class Refs extends React.Component {

    constructor() {
        super();
        this.state = {
            red: 128,
            green: 128,
            blue: 128
        }
        this.update = this.update.bind(this)
    }

    update() {
        this.setState({
            red: ReactDOM.findDOMNode(this.refs.red).value,
            green: ReactDOM.findDOMNode(this.refs.green).value,
            blue: ReactDOM.findDOMNode(this.refs.blue).value
        })
    }

    render() {
        let divStyle = {
            width: '100px',
            height: '100px',
            backgroundColor: 'rgb(' + this.state.red + ', ' + this.state.green + ', ' + this.state.blue + ')'
        };

        return (
            <div>
                Red:
                <Slider ref="red" update={this.update} />
                {this.state.red}
                <br/>

                Green:
                <Slider ref="green" update={this.update} />
                {this.state.green}
                <br/>

                Blue:
                <Slider ref="blue" update={this.update} />
                {this.state.blue}
                <br/>

                <div style={divStyle}/>
            </div>
        )
    }
}



class Slider extends React.Component {
    render() {
        return (
            <input
                type="range"
                min="0"
                max="255"
                onChange={this.props.update}
            />
        )
    }
}

export default Refs;
