import React from 'react';

class Props extends React.Component {
    render() {
        return (
            <h1>{this.props.txt}</h1>
        )
    }
}

// Props.propTypes = {
//     txt: React.PropTypes.string
//     //category: React.PropTypes.number.isRequired
// }

// Props.defaultProps = {
//     txt: 'This is the components default text'
// }

export default Props;
