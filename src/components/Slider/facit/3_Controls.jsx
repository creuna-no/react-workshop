import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeImage: 0
        }
    }

    setImage = (image) => {
        this.setState({
            image
        });
    }

    next = () => {
        if(this.props.images[this.state.activeImage+1]) this.setImage(this.state.activeImage+1);
    }

    prev = () => {
        if(this.props.images[this.state.activeImage-1]) this.setImage(this.state.activeImage-1);
    }

    render() {
        const { state, props: { images } } = this;

        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {images.map((image, key) => (
                            <li key={key}>
                                <img style={{display: key === state.activeImage ? 'block' : 'none'}} src={image} />
                            </li>
                        ))}
                    </ul>
                    {this.props.controls && (
                        <ul className="slider__controls">
                            <li className="prev"><button onClick={this.prev}>&lt;</button></li>
                            <li className="next"><button onClick={this.next}>&gt;</button></li>
                        </ul>
                    )}
                </div>
            </div>
        )
    }
}

Slider.defaultProps = {
    controls: true
}

Slider.propTypes = {
    controls: React.PropTypes.bool.isRequired,
    images: React.PropTypes.array
}

export default Slider;
