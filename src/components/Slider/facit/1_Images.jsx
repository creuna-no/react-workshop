import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);

        // STEP: 2
        // Setup the initial state to have an image property
        // which has the default value 0
        // (This property will be the active image in the slider)
    }

    render() {
        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {this.props.images.map((image, i) => (
                            <li key={i}>
                                {/*
                                    STEP: 3
                                    When looping through all images, make a check against
                                    the "image" property in the state and only display the active image
                                */}
                                <img src={image} />
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}

Slider.propTypes = {
    images: React.PropTypes.array
}

export default Slider;
