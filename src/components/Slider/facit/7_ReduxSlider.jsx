import React, { Component } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import * as appActions from '../../actions/appActions';

class ReduxSlider extends Component {
    setImage = (image) => this.props.actions.setImage.setImage(image);
    next = () => this.props.actions.nextImage.nextImage();
    prev = () => this.props.actions.nextImage.prevImage();

    render() {
        const {activeImage, images, loop, pager, controls } = this.props;

        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {images.map((image, key) => (
                            <li key={key}>
                                <img style={{display: key === activeImage ? 'block' : 'none'}} src={image} />
                            </li>
                        ))}
                    </ul>
                    {controls && (
                        <ul className="slider__controls">
                            {(activeImage > 0 || loop) && (
                                <li className="prev"><button onClick={this.prev}>&lt;</button></li>
                            )}
                            {(images.length-1 > activeImage || loop) && (
                                <li className="next"><button onClick={this.next}>&gt;</button></li>
                            )}
                        </ul>
                    )}
                </div>
                {pager && (
                    <ul className="slider__pager">
                        {images.map((image, key) => (
                            <li key={key}>
                                <button
                                    disabled={activeImage === key}
                                    className={activeImage === key ? 'active' : null}
                                    onClick={() => { this.setImage(key) }}
                                >{key+1}</button>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        )
    }
}

ReduxSlider.defaultProps = {
    controls: true,
    pager: true,
    loop: true
}

ReduxSlider.propTypes = {
    controls: React.PropTypes.bool.isRequired,
    pager: React.PropTypes.bool.isRequired,
    loop: React.PropTypes.bool.isRequired,
    images: React.PropTypes.array,
    activeImage: React.PropTypes.number,
    actions: React.PropTypes.object
}

export default connect(
        state => ({
            activeImage: state.activeImage,
            images: state.images,
            loop: state.loop,
            controls: state.controls,
            pager: state.pager
        }),
        dispatch => ({
            actions: {
                setImage: bindActionCreators(appActions, dispatch),
                nextImage: bindActionCreators(appActions, dispatch),
                prevImage: bindActionCreators(appActions, dispatch)
            }
        })
    )(ReduxSlider)
