import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeImage: 0
        }
    }

    setImage = (activeImage) => {
        this.setState({
            activeImage
        });
    }

    browse = (rotation) => {
        const { images, loop } = this.props;
        const { activeImage } = this.state;

        switch(rotation)
        {
            case 'next':
                if(images[activeImage+1]) {
                    this.setImage(activeImage+1);
                } else if(loop) {
                    this.setImage(0);
                }
                break;

            case 'prev':
                if(images[activeImage-1]) {
                    this.setImage(activeImage-1);
                } else if(loop) {
                    this.setImage(images.length - 1);
                }
                break;
        }
    }

    next = () => {
        this.browse('next')
    }

    prev = () => {
        this.browse('prev')
    }

    render() {
        const { state, props: { images, loop, controls, pager } } = this;

        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {images.map((image, key) => (
                            <li className={key === state.activeImage ? 'active' : ''} key={key}>
                                <img src={image} />
                            </li>
                        ))}
                    </ul>
                    {controls && (
                        <ul className="slider__controls">
                            {(state.activeImage > 0 || loop) && (
                                <li className="prev"><button onClick={this.prev}>&lt;</button></li>
                            )}
                            {(images.length-1 > state.activeImage || loop) && (
                                <li className="next"><button onClick={this.next}>&gt;</button></li>
                            )}
                        </ul>
                    )}
                </div>
                {pager && (
                    <ul className="slider__pager">
                        {images.map((image, key) => (
                            <li key={key}>
                                <button
                                    disabled={state.activeImage === key}
                                    className={state.activeImage === key ? 'active' : null}
                                    onClick={() => { this.setImage(key) }}
                                >{key+1}</button>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        )
    }
}

Slider.defaultProps = {
    controls: true,
    pager: true,
    loop: true
}

Slider.propTypes = {
    controls: React.PropTypes.bool.isRequired,
    pager: React.PropTypes.bool.isRequired,
    loop: React.PropTypes.bool.isRequired,
    images: React.PropTypes.array
}

export default Slider;
