import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeImage: 0
        }
    }

    setImage = (activeImage) => {
        this.setState({
            activeImage
        });
    }

    next = () => {
        if(this.props.images[this.state.activeImage+1]) {
            this.setImage(this.state.activeImage+1);
        } else if(this.props.loop) {
            this.setImage(0);
        }
    }

    prev = () => {
        if(this.props.images[this.state.activeImage-1]) {
            this.setImage(this.state.activeImage-1);
        } else if(this.props.loop) {
            this.setImage(this.props.images.length - 1);
        }
    }

    render() {
        const { state, props: { images, loop } } = this;

        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {images.map((image, key) => (
                            <li key={key}>
                                <img style={{display: key === state.activeImage ? 'block' : 'none'}} src={image} />
                            </li>
                        ))}
                    </ul>
                    {this.props.controls && (
                        <ul className="slider__controls">
                            {(state.activeImage > 0 || loop) && (
                                <li className="prev"><button onClick={this.prev}>&lt;</button></li>
                            )}
                            {(images.length-1 > state.activeImage || loop) && (
                                <li className="next"><button onClick={this.next}>&gt;</button></li>
                            )}
                        </ul>
                    )}
                </div>
            </div>
        )
    }
}

Slider.defaultProps = {
    controls: true,
    loop: true
}

Slider.propTypes = {
    controls: React.PropTypes.bool.isRequired,
    loop: React.PropTypes.bool.isRequired,
    images: React.PropTypes.array
}

export default Slider;
