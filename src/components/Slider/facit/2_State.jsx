import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeImage: 0
        }
    }

    // STEP: 4
    // Add 3 methods
    // 1. "setImage" - This should update the state with the provided value
    // 2. "next" - This should check if there is a "next" image and then call the "setImage" method with the next image
    // 3. "prev" - Same as next, but check for the previous image

    render() {
        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {this.props.images.map((image, i) => (
                            <li key={i} style={{display: this.state.activeImage === i ? 'block' : 'none'}}>
                                <img src={image} />
                            </li>
                        ))}
                    </ul>
                    {/*
                        STEP: 5
                        Add a defaultProps declaration for this component, with a property called "controls" that has the value "true"

                        STEP: 6
                        Make a check if the controls props is true and then print an <ul> with the class "slider__controls"
                        which should contain two <li> elements which should have the classes "prev" and "next"

                        STEP: 7
                        Add buttons to the next/prev <li> elements make them trigger the next/prev methods
                        you created in step 4 when they are clicked.
                    */}
                </div>
            </div>
        )
    }
}

Slider.propTypes = {
    images: React.PropTypes.array
}

// BONUS STEP: Add a propType check for the "controls" property, which should be a bool and be a required property

export default Slider;
