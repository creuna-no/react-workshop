import React, { Component } from 'react'

// TODO: import: connect, bindActionCreators and actions
// TODO: export component through connect
// TODO: change internal state to redux props
// TODO: move logic into action file and trigger them from this component

class Slider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            image: 0
        }
    }

    setImage = (image) => {
        this.setState({
            image
        });
    }

    browse = (rotation) => {
        const { images, loop } = this.props;
        const { image } = this.state;

        switch(rotation)
        {
            case 'next':
                if(images[image+1]) {
                    this.setImage(image+1);
                } else if(loop) {
                    this.setImage(0);
                }
                break;

            case 'prev':
                if(images[image-1]) {
                    this.setImage(image-1);
                } else if(loop) {
                    this.setImage(images.length - 1);
                }
                break;
        }
    }

    next = () => {
        this.browse('next')
    }

    prev = () => {
        this.browse('prev')
    }

    render() {
        const { state, props: { images, loop } } = this;

        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {images.map((image, key) => (
                            <li className={key === state.image ? 'active' : ''} key={key}>
                                <img src={image} />
                            </li>
                        ))}
                    </ul>
                    {this.props.controls && (
                        <ul className="slider__controls">
                            {(state.image > 0 || loop) && (
                                <li className="prev"><button onClick={this.prev}>&lt;</button></li>
                            )}
                            {(images.length-1 > state.image || loop) && (
                                <li className="next"><button onClick={this.next}>&gt;</button></li>
                            )}
                        </ul>
                    )}
                </div>
                {this.props.pager && (
                    <ul className="slider__pager">
                        {images.map((image, key) => (
                            <li key={key}>
                                <button
                                    disabled={state.image === key}
                                    className={state.image === key ? 'active' : null}
                                    onClick={() => { this.setImage(key) }}
                                >{key+1}</button>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        )
    }
}

Slider.defaultProps = {
    controls: true,
    pager: true,
    loop: true
}

Slider.propTypes = {
    controls: React.PropTypes.bool.isRequired,
    pager: React.PropTypes.bool.isRequired,
    loop: React.PropTypes.bool.isRequired,
    images: React.PropTypes.array
}

export default Slider;
