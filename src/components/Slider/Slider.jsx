import React, { Component } from 'react'

class Slider extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="slider">
                <div className="slider__content">
                    <ul className="slider__images">
                        {/* Step 1 */}
                    </ul>
                </div>
            </div>
        )
    }
}

Slider.propTypes = {
    images: React.PropTypes.array
}

export default Slider;


/*
    STEP 1:
    Loop through all images here and output <li>'s with <img>'s
    TIP: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
*/

/*
    STEP 2:
    Add activeImage state to the component and only show the active image.
*/

/*
    STEP 3:
    Add next/prev controls to the component.

    ul class = 'slider__controls'
    li classes = 'prev' and 'next'

    Create functions that handles the prev/next functionality.
    Enable/disable controls based on prop
*/

/*
    STEP 4:
    Add loop to props
    Update prev/next functions to handle looping

    Disable prev/next button if first/last image
*/

/*
    STEP 5:
    Add pager to slider

    ul class = 'slider__pager'
    active class = 'active'

    Enable/disable pager based on prop
*/

/*
    STEP 6:
    Remove the active image style attribute and replace with className 'active'
    Insert animation styles in index.html
*/
