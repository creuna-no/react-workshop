import React from 'react';
import ReactDOM from 'react-dom';

// Redux
// import appStore from './store/appStore';
// import { Provider } from 'react-redux';

// Define our HTML element which we should
// render React to
const root = document.getElementById('app')

/****************
## REACT INTRO ##
****************/

// React render function
let render = () => {
    const App = require('./containers/Intro.jsx').default;
    ReactDOM.render(
        <App />,
        root
    )
}

/**********
## REACT ##
**********/

// // Access images array defined in the HTML
// const images = window.__PAGECONTENT.images;
//
// // React render function
// let render = () => {
//     const App = require('./containers/App.jsx').default;
//     ReactDOM.render(
//         <App images={images} />,
//         root
//     )
// }

/**********
## REDUX ##
**********/

// const store = appStore({
//     images: window.__PAGECONTENT.images,
//     activeImage: 0,
//     loop: true,
//     controls: true,
//     pager: false
// });
//
// // React render function
// let render = () => {
//     const App = require('./containers/ReduxApp.jsx').default;
//     ReactDOM.render(
//         <Provider store={store}>
//             <App />
//         </Provider>,
//         root
//     )
// }

// Hot reloading support
if (module.hot) {
    // Support hot reloading of components
    // and display an overlay for runtime errors
    const renderApp = render
    const renderError = (error) => {
        const RedBox = require('redbox-react')
        ReactDOM.render(
            <RedBox error={error} />,
            root
        )
    }
    render = () => {
        try {
            renderApp()
        } catch (error) {
            renderError(error)
        }
    }

    module.hot.accept('./containers/App.jsx', () => {
        setTimeout(render)
    })
}

// Make initial render
render()
