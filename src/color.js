import React from 'react'
import ReactDOM from 'react-dom'

// Define our HTML element which we should
// render React to
const root = document.getElementById('app')

// React render function
let render = () => {
    const App = require('./containers/Color.jsx').default;
    ReactDOM.render(
        <App />,
        root
    )
}

// Hot reloading support
if (module.hot) {
    // Support hot reloading of components
    // and display an overlay for runtime errors
    const renderApp = render
    const renderError = (error) => {
        const RedBox = require('redbox-react')
        ReactDOM.render(
            <RedBox error={error} />,
            root
        )
    }
    render = () => {
        try {
            renderApp()
        } catch (error) {
            renderError(error)
        }
    }

    module.hot.accept('./containers/Color.jsx', () => {
        setTimeout(render)
    })
}

// Make initial render
render()
