const initialState = {
    loop: true,
    controls: true,
    pager: false,
    activeImage: 0,
    images: []
}

export default function slider(state = initialState, action) {
    switch (action.type) {
        case 'SET_IMAGE':
            return {...state, activeImage: action.image}

        case 'SET_IMAGES':
            return {...state, images: action.images}

        default:
            return state;
    }
}
