export function setImage(image) {
    if(image >= 0) {
        return {
            type: "SET_IMAGE",
            image
        }
    }
}

export function nextImage() {
    return (dispatch, getState) => {
        const {images, activeImage, loop} = getState();

        let image = images[activeImage+1] ? activeImage + 1 : loop ? 0 : null;

        dispatch(setImage(image));
    }
}

export function prevImage() {
    return (dispatch, getState) => {

        const {images, activeImage, loop} = getState();

        let image = images[activeImage-1] ? activeImage - 1 : loop ? images.length - 1 : null;

        dispatch(setImage(image));
    }
}

export function fetchImages() {
    return (dispatch) => {
        window.fetch(`/api/images`)
            .then(res => res.json().then(json => {
                dispatch(setImages(json.images))
            }))
            .catch(exception => console.error(exception))
    }
}

function setImages(images) {
    return {
        type: 'SET_IMAGES',
        images
    }
}
