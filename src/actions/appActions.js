export function setImage(image) {
    // Check if we have an image
    return {
        // action
    }
}

export function nextImage() {
    return (dispatch, getState) => {
        // get the props we need from state

        // Do logic for next image

        dispatch(
            // action
        );
    }
}

export function prevImage() {
    return (dispatch, getState) => {

        // get the props we need from state

        // Do logic for prev image

        dispatch(
            // action
        );
    }
}

// Redux Phase 2 - API

export function fetchImages() {
    return (dispatch) => {
        // fetch the images from the API endpoint "/api/images"
        // and then pass the images from the json to the
        // setImages() function as an argument
    }
}

function setImages(images) {
    // return a SET_IMAGES action along with the provided images attribute
    // Add a case for this action in the reducer
    return {
        // action
    }
}
