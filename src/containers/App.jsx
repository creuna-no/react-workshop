import React, { Component } from 'react';
import { Slider } from '../components/Slider';

class App extends Component {
    render() {
        const { images } = this.props;

        return (
            <main>
                <h1>React Slider</h1>
                <Slider images={images} />
            </main>
        );
    }
}

App.propTypes = {
    images: React.PropTypes.array
}

export default App;
