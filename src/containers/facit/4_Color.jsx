import React, { Component } from 'react';
import ColorPicker, { SliderPicker } from 'react-color';

class Color extends Component {
    constructor(props) {
        super(props);

        this.state = {
            primary: '#FC0',
            secondary: '#111'
        }
    }

    _handlePrimary = (value) => {
        this.updateColor('primary', value.hex)
    }

    _handleSecondary = (value) => {
        this.updateColor('secondary', value.hex)
    }

    updateColor(color, value) {
        this.setState({
            [color]: '#'+value
        })
    }


    render() {
        return (
            <main>
                <h1 style={{
                    padding: '10px',
                    color: this.state.primary,
                    backgroundColor: this.state.secondary
                }}>Color</h1>
                <div>
                    <h2>Primary color</h2>
                    <ColorPicker color={this.state.primary} onChange={this._handlePrimary} />
                </div>
                <div>
                    <h2>Secondary color</h2>
                    <SliderPicker color={this.state.secondary} onChange={this._handleSecondary} />
                </div>
            </main>
        );
    }
}

export default Color;
