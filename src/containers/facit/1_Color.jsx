import React, { Component } from 'react';
import ColorPicker from 'react-color'; // npm install react-color

class Color extends Component {
    constructor(props) {
        super(props);

        this.state = {
            primary: '#FC0',
            secondary: '#111'
        }
    }

    render() {
        return (
            <main>
                <h1 style={{
                    padding: '10px',
                    color: this.state.primary,
                    backgroundColor: this.state.secondary
                }}>Color</h1>
                {/*
                    STEP: 2
                    Print a <div> along with a
                    header and a ColorPicker component.

                    The ColorPicker should have the property
                    "color" which should be the primary color in our state
                */}
            </main>
        );
    }
}

export default Color;
