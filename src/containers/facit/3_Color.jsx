import React, { Component } from 'react';
import ColorPicker from 'react-color';

class Color extends Component {
    constructor(props) {
        super(props);

        this.state = {
            primary: '#FC0',
            secondary: '#111'
        }
    }

    // STEP: 5
    // Add a second method for updating the secondary value
    // and also refactor the primary method so that
    // it uses a "updateColor" method which should accept
    // two arguments, one for the color type (primary/secondary)
    // and one for the color value
    // (Both the primary and secondary handlers should use the "updateColor" method)

    _handlePrimary = (color) => {
        this.setState({
            primary: '#'+color.hex
        })
    }

    render() {
        return (
            <main>
                <h1 style={{
                    padding: '10px',
                    color: this.state.primary,
                    backgroundColor: this.state.secondary
                }}>Color</h1>
                <div>
                    <h2>Primary color</h2>
                    <ColorPicker color={this.state.primary} onChange={this._handlePrimary} />
                </div>
                {/*
                    STEP: 6
                    Add a second color picker which should use the SliderPicker
                    component from the "react-color" library

                    It should also use the _handleSecondary method you created
                */}
            </main>
        );
    }
}

export default Color;
