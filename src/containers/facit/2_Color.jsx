import React, { Component } from 'react';
import ColorPicker from 'react-color';

class Color extends Component {
    constructor(props) {
        super(props);

        this.state = {
            primary: '#FC0',
            secondary: '#111'
        }
    }

    // STEP: 3
    // Add a _handlePrimary method which should accept
    // a "color" argument and then set the hex value
    // to the "primary" state attribute
    // (You may need to prepend a # symbol to this value)

    render() {
        return (
            <main>
                <h1 style={{
                    padding: '10px',
                    color: this.state.primary,
                    backgroundColor: this.state.secondary
                }}>Color</h1>
                <div>
                    <h2>Primary color</h2>
                    <ColorPicker color={this.state.primary} />
                    {/*
                        STEP: 4
                        Add the "onChange" property to the ColorPicker
                        and bind it to the _handlePrimary method
                    */}
                </div>
            </main>
        );
    }
}

export default Color;
