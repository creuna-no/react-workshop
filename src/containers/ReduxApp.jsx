import React, { Component } from 'react';
import { ReduxSlider } from '../components/Slider';

class ReduxApp extends Component {
    render() {
        return (
            <main>
                <h1>React Slider</h1>
                <ReduxSlider />
            </main>
        );
    }
}

export default ReduxApp;
