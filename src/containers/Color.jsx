import React, { Component } from 'react';
// STEP: 1
// Install the react-color library (https://github.com/casesandberg/react-color)
// and import the ColorPicker component

class Color extends Component {
    constructor(props) {
        super(props);

        this.state = {
            primary: '#FC0',
            secondary: '#111'
        }
    }

    render() {
        return (
            <main>
                <h1 style={{
                    padding: '10px',
                    color: this.state.primary,
                    backgroundColor: this.state.secondary
                }}>Color</h1>
            </main>
        );
    }
}

export default Color;
