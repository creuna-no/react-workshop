import React, { Component } from 'react';

import { HelloWorld, Props, State, Children, Refs, Lifecycle, LifecycleUpdating } from '../components/Intro';

class Intro extends Component {

    // constructor() {
    //     super();
    //
    //     this.state = {
    //         alert: false
    //     }
    //
    //     this.showAlert = this.showAlert.bind(this);
    //     this.hideAlert = this.hideAlert.bind(this);
    // }
    //
    // showAlert() {
    //     this.setState({alert: true})
    // }
    // 
    // hideAlert() {
    //     this.setState({alert: false})
    // }

    render() {
        return (
            <main>
                <HelloWorld/>
                {/* <LifecycleUpdating alert={this.state.alert} showAlert={this.showAlert} hideAlert={this.hideAlert} /> */}
            </main>
        );
    }
}

export default Intro;
